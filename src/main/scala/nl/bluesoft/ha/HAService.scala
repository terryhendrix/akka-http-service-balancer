package nl.bluesoft.ha

import akka.actor._
import akka.event.{Logging, LoggingAdapter}
import akka.util.Timeout
import scala.collection.JavaConversions._
import scala.concurrent.duration._
import spray.json.DefaultJsonProtocol
import com.typesafe.config._
import spray.client.pipelining._
import spray.http.{HttpRequest, BasicHttpCredentials}
import scala.util.Failure
import akka.pattern.ask
import scala.concurrent.{Promise, ExecutionContext, Future}
import java.util.concurrent.TimeUnit
import spray.httpx.marshalling.MetaMarshallers
import nl.bluesoft.ha.actorsystem.{MessagingV1, HostManager}
import nl.bluesoft.ha.actorsystem.MessagingV1.CallService

trait HighAvailabilityService[ServiceType <: Extension] extends ExtensionId[ServiceType] with ExtensionIdProvider

abstract class HighAvailabilityServiceImpl[ServiceType](system: ExtendedActorSystem) extends Extension
{
  def serviceName: String
  implicit val ec:ExecutionContext

  implicit val actorRefFactory:ActorRefFactory = system
  implicit val log:LoggingAdapter = Logging(system, "Webservice "+serviceName)
  lazy val settings: HighAvailabilityServiceSettings = new HighAvailabilityServiceSettings(system, serviceName)(log)
  implicit val hostManagerTimeout:Timeout = Timeout(settings callTimeout)
  lazy val hostTimeout: Timeout = Timeout(settings hostTimeout)

  val hostManager = system.actorOf(Props(new HostManager(serviceName)), s"$serviceName-HostManager")

  def define(call:CallDefinition): Future[Any] = hostManager ask CallService(call)
}
