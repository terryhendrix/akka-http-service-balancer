package nl.bluesoft.ha.actorsystem
import scala.concurrent.duration._
import akka.actor.{Cancellable, ActorRef, FSM}
import nl.bluesoft.ha.{DefinitionExecutor, CallDefinition, Host}
import scala.concurrent.Await
import nl.bluesoft.ha.actorsystem.MessagingV1.{CheckHost, HostIsDown}


case class MoveDown(failedMsg:MessagingV1.CallService, ref:ActorRef)
case object MoveUp

object HAHost
{
  sealed trait State
  case object Up extends State
  case object Down extends State
}

import HAHost._
class HAHost(host:Host) extends FSM[State, Unit]
{
  override implicit val log = super.log
  implicit def cancellableToOptionCancellable(c:Cancellable) = Option(c)
  implicit val ec = context.dispatcher
  log.info(s"Spawning HAHost for $host")
  startWith(Up, Unit)
  var cancellable:Option[Cancellable] = None

  when(Up) {
    case Event(msg: MessagingV1.CallService, _) =>
      val ref = sender()
      DefinitionExecutor.execute(msg.definition, host) map {
        ref ! _ // forward response
      } recover {
        case _ => self ! MoveDown(msg, ref)
      }
      stay()
    case(Event(MoveDown(msg,ref),_)) =>
      context.parent ! MessagingV1.HostIsDown
      context.parent.tell(msg,ref)
      cancellable = Option(context.system.scheduler.scheduleOnce(1 second, self, MoveUp))
      log.info(s"Host ${host.address} is [DOWN]")
      goto(Down)
  }

  when(Down) {
    case Event(msg:MessagingV1.CallService, _) =>
      cancellable = context.system.scheduler.schedule(1 second, 1 second, self, CheckHost)
      context.parent.tell(msg,sender())
      sender ! MessagingV1.HostIsDown
      stay()
    case Event(MoveUp,_) =>
      log.info(s"Host ${host.address} is back [UP]")
      context.parent ! MessagingV1.HostIsUp
      cancellable map(_.cancel())
      goto(Up)
    case Event(CheckHost, _) =>
      self ! MoveUp
      cancellable map(_.cancel())
      stay()
  }
}
