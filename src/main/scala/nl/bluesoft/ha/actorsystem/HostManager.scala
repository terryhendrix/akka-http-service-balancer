package nl.bluesoft.ha.actorsystem

import scala.concurrent.ExecutionContext
import akka.actor.{Props, ActorRef, ActorLogging, Actor}
import scala.concurrent.duration.{FiniteDuration, Duration}
import java.util.concurrent.TimeUnit
import nl.bluesoft.ha.{Host, HighAvailabilityServiceSettings}
import akka.routing.{Routee, ActorRefRoutee, RoundRobinRoutingLogic, Router}

/**
 * Created by terryhendrix on 12/11/14.
 */
class HostManager(serviceName:String)(implicit ec:ExecutionContext) extends Actor with ActorLogging
{
  implicit def durationToFiniteDuration(duration:Duration):FiniteDuration = FiniteDuration(duration.toMillis, TimeUnit.MILLISECONDS)
  val settings: HighAvailabilityServiceSettings = new HighAvailabilityServiceSettings(context.system, serviceName)(log)
  val routingNodes = settings.hosts.map[Routee,Vector[Routee]] {
    configuredHost =>
      val ref = context.actorOf(Props(new HAHost(configuredHost)), s"HAHost-${configuredHost.name}")
      context watch ref
      ActorRefRoutee(ref)
  }
  var router = Router(RoundRobinRoutingLogic(), routingNodes )

  override def receive: Receive = {
    case msg:MessagingV1.CallService =>
      router.route(msg, sender())
    case MessagingV1.HostIsDown =>
      downHost(sender())
    case MessagingV1.HostIsUp =>
      upHost(sender())
    case any => log.info(s"Received unhandled message: $any")
  }

  def downHost(ref:ActorRef) = {
    log.debug(s"Removing routee")
    router = router.removeRoutee(ref)
  }

  def upHost(ref:ActorRef) = {
    log.debug(s"Adding routee")
    router = router.addRoutee(ref)
  }

  def actorName(ref:ActorRef) = ref.path.toString.split("/").reverse.head
}
