package nl.bluesoft.ha.actorsystem

import nl.bluesoft.ha.{Host, CallDefinition}


/**
 * Created by terryhendrix on 12/11/14.
 */
object MessagingV1 {
  case object HostIsDown
  case object HostIsUp
  case object CheckHost
  case class CallService(definition:CallDefinition)
}
