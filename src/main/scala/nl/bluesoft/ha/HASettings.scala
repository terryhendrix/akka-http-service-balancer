package nl.bluesoft.ha

import com.typesafe.config.Config
import akka.event.LoggingAdapter
import scala.concurrent.duration.{FiniteDuration, Duration}
import scala.collection.JavaConversions._
import java.util.concurrent.TimeUnit
import akka.actor.ActorSystem
import java.net.InetAddress

/**
 * Created by terryhendrix on 12/11/14.
 */
case class Host(name:String, protocol:String, host:String, port:Int, path:String, username:Option[String], password:Option[String])
{
  val httpAddress = s"${protocol}://${host}:${port}/${path}"
  val address = s"$host:$port"
}

case class HighAvailabilityServiceSettings(hosts:Vector[Host], resetInterval:FiniteDuration, hostTimeout:FiniteDuration, callTimeout:FiniteDuration)(implicit log:LoggingAdapter)
{
  log.info(s"HighAvailabilityServiceSettings($hosts)")

  def this(tuple:(Vector[Host], FiniteDuration, FiniteDuration, FiniteDuration))(implicit log:LoggingAdapter) = {
    this(tuple._1, tuple._2, tuple._3, tuple._4)
  }

  def this(system:ActorSystem, webserviceName: String)(implicit log:LoggingAdapter) = {
    this(ConfigReader.readConfig(system.settings.config.getConfig(s"services.$webserviceName")))
  }
}

object ConfigReader
{
  implicit def anyToString(any:Any) = any.toString
  implicit def anyOptionToStringOption(any:Option[Any]) = any map { _.toString }
  implicit def anyToInt(any:Any) = any.toString.toInt
  implicit def durationToFiniteDuration(duration:Duration):FiniteDuration = FiniteDuration(duration.toMillis, TimeUnit.MILLISECONDS)
  implicit def stringToFiniteDuration(duration:String):FiniteDuration = FiniteDuration(Duration(duration).toMillis, TimeUnit.MILLISECONDS)

  def readConfig(config:Config)(implicit log:LoggingAdapter): (Vector[Host], FiniteDuration, FiniteDuration, FiniteDuration) = {
    var hosts = Vector.empty[Host]
    config.getList("hosts").unwrapped().foreach {
      case configured:java.util.HashMap[String, Any] =>
        val map = configured.toMap
        val host = Host(map("name"),map("protocol"),map("host"),map("port"),map("path"), map.get("username"), map.get("password"))
        hosts = hosts :+ host
    }
    (hosts, config.getString("check-interval"), config.getString("host-timeout"), config.getString("call-timeout"))
  }
}