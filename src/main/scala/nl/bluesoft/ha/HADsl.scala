package nl.bluesoft.ha
import spray.json.DefaultJsonProtocol
import spray.httpx.marshalling.{Marshaller, MetaMarshallers}
import akka.event.{Logging, LoggingAdapter}
import akka.actor.ActorRefFactory
import scala.concurrent.{Future, ExecutionContext}
import akka.util.Timeout
import spray.json._
import spray.client.pipelining._
import spray.http.{BasicHttpCredentials, HttpRequest}
import nl.bluesoft.ha.actorsystem.HAHost

/**
 * Created by terryhendrix on 12/11/14.
 */

case class HostTriest(amount:Int)
case class MaxTryDefinition(maxTries:Int)
case class ExceptionDefinition(recover:(Throwable) => Unit)

case class HostTriesInt(amount:Int) {
  def hosts = HostTriest(amount)
  def host = HostTriest(amount)
}

case class CompleteDefinition(complete:(Host) => Future[Any], hostDown:Option[ExceptionDefinition]=None)
{
  def ~ (hostDown:ExceptionDefinition) = copy(hostDown = Some(hostDown))
  private[ha] def tryComplete(host:Host) = complete(host)
}

case class CallDefinition(complete:CompleteDefinition, maxTryDef:MaxTryDefinition)
{
  def ~ (maxTryDef:MaxTryDefinition):CallDefinition = this.copy(maxTryDef = maxTryDef)
  private[ha] def tryComplete(host:Host) = complete.complete(host)
}

trait HighAvailabilityDsl extends DefaultJsonProtocol with MetaMarshallers
{
  implicit def hostDownToOption(hostDownDef:ExceptionDefinition) = Option(hostDownDef)

  implicit def log:LoggingAdapter
  implicit val actorRefFactory:ActorRefFactory
  implicit val ec:ExecutionContext
  implicit val hostTimeout:Timeout

  implicit def intToTriesInt(amount:Int) = HostTriesInt(amount)
  implicit def intToTries(amount:Int) = HostTriest(amount)

  def attempt(maxTries:HostTriest = HostTriest(5))(complete:CompleteDefinition):CallDefinition = {
    CallDefinition(
      complete = complete,
      maxTryDef = MaxTryDefinition(maxTries.amount)
    )
  }

  def maxTries(tries:Int) = MaxTryDefinition(tries)


  def defaultPipelineNoAuth: HttpRequest => Future[String] = {
    logRequest(log, Logging.InfoLevel) ~>
    sendReceive(actorRefFactory, ec, hostTimeout) ~>
    logResponse(log, Logging.InfoLevel) ~>
    unmarshal[String]
  }

  def defaultPipelineWithAuth(username:String, password:String): HttpRequest => Future[String] = {
    addCredentials(BasicHttpCredentials(username, password)) ~>
    logRequest(log, Logging.InfoLevel) ~>
    sendReceive(actorRefFactory, ec, hostTimeout) ~>
    logResponse(log, Logging.InfoLevel) ~>
    unmarshal[String]
  }

  def defaultPipeline(host:Host): HttpRequest => Future[String] = {
    val pipeline = for {
      username <- host.username
      password <- host.password
    } yield defaultPipelineWithAuth(username, password)
    pipeline.getOrElse(defaultPipelineNoAuth)
  }

  def recover(f:Throwable => Unit):ExceptionDefinition = {
    ExceptionDefinition(f)
  }

  def get(path:String)(f:String => Any):CompleteDefinition = {
    CompleteDefinition(host =>
      defaultPipeline(host)(Get(s"${host.httpAddress}/$path")
    ) map { case s:String => f(s) } )
  }

  def delete(path:String)(f:String => Any):CompleteDefinition = {
    CompleteDefinition(host =>
      defaultPipeline(host)(Delete(s"${host.httpAddress}/$path")
    ) map { case s:String => f(s) } )
  }

  def post[T](path:String, body:T)(f:String => Any)(implicit writer:Marshaller[T]):CompleteDefinition = {
    CompleteDefinition(host =>
      defaultPipeline(host)(Post(s"${host.httpAddress}/$path", body)
    ) map { case s:String => f(s) } )
  }

  def put[T](path:String, body:T)(f:String => Any)(implicit writer:Marshaller[T]):CompleteDefinition = {
    CompleteDefinition(host =>
      defaultPipeline(host)(Put(s"${host.httpAddress}/$path", body)
    ) map { case s:String => f(s) } )
  }
}

object DefinitionExecutor
{
  def execute(_def:CallDefinition, host:Host)(implicit ec:ExecutionContext, log:LoggingAdapter):Future[Any] = {
    _def tryComplete host recover {
      case ex:Throwable =>
        log.error("Error while execution a call definition")
        ex.printStackTrace()
        _def.complete.hostDown.map { exDef =>
        exDef.recover(ex)
        throw ex
      }
    }
  }
}