package nl.bluesoft.ha

import akka.actor.{ExtendedActorSystem, Extension, ExtensionId}

import spray.client.pipelining._
import scala.concurrent.Future
import scala.util.Failure
import nl.bluesoft.ha.HighAvailabilityService
import akka.util.Timeout

/**
 * Created by terryhendrix on 23/10/14.
 */
object TestHAService extends HighAvailabilityService[TestHAService]
{
  override def createExtension(system: ExtendedActorSystem) = new TestHAService(system)
  override def lookup(): ExtensionId[_ <: Extension] = TestHAService
}

class TestHAService(system:ExtendedActorSystem) extends HighAvailabilityServiceImpl(system:ExtendedActorSystem) with HighAvailabilityDsl
{
  override def serviceName: String = "tractracer-cluster"
  override implicit val ec = system.dispatcher

  def ping():Future[Boolean] = {
    define {
      attempt(5 hosts) {
        get("ping") { result =>
          result == "true"
        } ~ recover { ex =>
          log.info("Email sent that host is down")
        }
      }
    }.mapTo[Boolean]
  }
}