package nl.bluesoft.ha

import akka.testkit.TestKit
import akka.actor.ActorSystem
import org.scalatest.FlatSpecLike
import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by terryhendrix on 09/11/14.
 */
class HighAvailabilityServiceTest extends TestKit(ActorSystem("TestCluster")) with FlatSpecLike
{
  val service = TestHAService(system)

  "An HighAvailabilityService" should "ping round robin" in {
    for{ i <- Range(0,1000) } {
      val res = Await.result(service.ping(), service.settings.callTimeout)  // test will allow time for checking ~5 hosts
      println(res)
    }
  }
}