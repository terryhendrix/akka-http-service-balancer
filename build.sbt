import akka.sbt.AkkaKernelPlugin._
import sbt.Keys._

organization := "nl.bluesoft"

name := "akka-ha-service"

version := "1.1.2"

scalaVersion := "2.11.2"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

libraryDependencies ++=
  {
    val akkaV = "2.3.4"
    val sprayV = "1.3.1"
    val jsonV = "1.2.6"
    Seq(
      "org.scala-lang"       % "scala-library"                  % scalaVersion.value,
      "com.typesafe.akka"   %% "akka-kernel"                    % akkaV,
      "com.typesafe.akka"   %% "akka-actor"                     % akkaV,
      "com.typesafe.akka"   %% "akka-slf4j"                     % akkaV,
      "com.typesafe.akka"   %% "akka-cluster"                   % akkaV,
      "com.typesafe.akka"   %% "akka-contrib"                   % akkaV,
      "com.typesafe.akka"   %% "akka-persistence-experimental"  % akkaV,
      "io.spray"            %% "spray-http"                     % sprayV,
      "io.spray"            %% "spray-httpx"                    % sprayV,
      "io.spray"            %% "spray-routing"                  % sprayV,
      "io.spray"            %% "spray-util"                     % sprayV,
      "io.spray"            %% "spray-io"                       % sprayV,
      "io.spray"            %% "spray-can"                      % sprayV,
      "io.spray"            %% "spray-client"                   % sprayV,
      "io.spray"            %% "spray-json"                     % jsonV,
      "ch.qos.logback"       % "logback-classic"                % "1.1.2",
      "com.typesafe.akka"   %% "akka-testkit"                   % akkaV   % "test",
      "org.scalatest"       %% "scalatest"                      % "2.1.4" % "test",
      "nl.bluesoft"         %% "akka-persistence-inmemory"      % "1.0.0" % "test"
    )
  }

autoCompilerPlugins := true

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

publishMavenStyle := true

publishArtifact in Test := false

fork in test := true

// Akka Kernel Plugin settings
akka.sbt.AkkaKernelPlugin.distSettings

distJvmOptions in Dist := "-Xms64M -Xmx128M -DVERSION=" + version.value

distMainClass in Dist := "akka.kernel.Main nl.bluesoft.tractracer.mytractracer.LaunchKernel"
